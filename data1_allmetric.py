import json
import numpy as np
import matplotlib.pyplot as plt
# from itertools import accumulate

def read_data():
    large_dataset_1 = {}
    with open('anomaly_data_z4.out', 'r') as f:
        for line in f:
            datapoints = []
            d = json.loads(line)
            try:
                target = d[0]['target']
                datapoints.extend(d[0]['datapoints'])
                large_dataset_1[target] = datapoints
            except IndexError: # there s a line with no metric and data somewhere
                print('skip empty data line')
    return large_dataset_1

def get_latest_moving_avg(history_data_input):
    '''
    history_data_input: a list with elements as pair of data and time points like [[data_1,timestamp_1] ... [data_n,timestamp_n]]
    null: all null will be remove s.t the average only moves with timestamp that have actual data
    return a single moving average up to the latest timestamp or NaN if data points are all null in history
    '''
    data_without_null = [data[0] for data in history_data_input if data[0] is not None]
    if data_without_null:
        return sum(data_without_null)/len(data_without_null)
    else:
        return np.nan

def get_latest_sd(history_data_input):
    '''
    history_data_input: a list with elements as pair of data and time points like [[data_1,timestamp_1] ... [data_n,timestamp_n]]
    null: all null will be remove s.t the average only moves with timestamp that have actual data
    return a single SD up to the latest timestamp or NaN if data points are all null in history
    '''
    data_without_null = [data[0] for data in history_data_input if data[0] is not None]
    if data_without_null:
        return np.std(data_without_null)
    else:
        return np.nan

def get_z(x,mean,sd):
    if x and mean != np.nan and sd != 0:
        return abs((x-mean)/sd)
    else:
        return np.nan

def process_data():
    '''
    take in all the data points in dataset1 except for last one and return the Z value of the latest point for all metrics
    however, last data point for most of the metric are null so result in lots of null
    '''
    dataset = read_data()
    all_metrics_latest_avg = {metric:get_latest_moving_avg(dataset[metric][:-5]) for metric in dataset}
    all_metrics_latest_sd = {metric:get_latest_sd(dataset[metric][:-5]) for metric in dataset}
    all_mettric_latest = {metric:dataset[metric][-5][0] for metric in dataset}
    all_metric_Z = {metric:get_z(all_mettric_latest[metric],all_metrics_latest_avg[metric],all_metrics_latest_sd[metric]) for metric in dataset}
    return all_metric_Z

def plotting():
    z_dict_all_metrics = process_data()
    data = np.array([list(z_dict_all_metrics.values())[:79],list(z_dict_all_metrics.values())[79:158],list(z_dict_all_metrics.values())[158:237],list(z_dict_all_metrics.values())[237:316],list(z_dict_all_metrics.values())[-79:]]) #all site 5 metrics
    print(data.shape)
    x_ax_label = [metric[6:] for metric in list(z_dict_all_metrics.keys())[:79]]
    y_ax_label = ['site1','site2','site3','site4','site5']
    fig, ax = plt.subplots(figsize=(15,6))
    im = ax.imshow(data)
    ax.set_xticks(np.arange(len(x_ax_label)))
    ax.set_yticks(np.arange(len(y_ax_label)))
    ax.set_xticklabels(x_ax_label)
    ax.set_yticklabels(y_ax_label)
    ax.tick_params(top=True, bottom=False,left=True,right=False,labeltop=True, labelbottom=False, labelleft=True,labelright=False)
    plt.setp(ax.get_xticklabels(), rotation=30, ha="left",rotation_mode="anchor",fontsize=6)
    # plt.setp(ax.get_yticklabels(), fontsize=2)

    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(len(x_ax_label)+1)-0.5,minor=True)
    ax.set_yticks(np.arange(len(y_ax_label)+1)-0.5,minor=True)
    ax.grid(which='minor',color="k", linestyle='-', linewidth=1)
    ax.tick_params(which="minor", bottom=False,left=False)

    for i in range(len(y_ax_label)):
        for j in range(len(x_ax_label)):
            text = ax.text(j, i, round(data[i,j],2),ha="center", va="center", color="grey",fontsize=6)
    fig.tight_layout()
    plt.show()

if __name__ == "__main__":
    # read_data()
    plotting() #which will create very squeeze but ok image of all metrics
    # process_data()
