# Anomaly detection
* [Wiki page for anomaly detection](https://en.wikipedia.org/wiki/Anomaly_detection):
  * unsupervised: data without label, find event that fit least to majority, this seems to be our case
  * supervised: data with label and model by training a classifier
  * semi-supervised: train a normal behaviour and conduct likelihood test
* Methods:
  1. Moving average
    * [wiki page for moving average](https://en.wikipedia.org/wiki/Moving_average) gives three types: simple MA, cumulative MA, weighted MA. Mathematically, Weighted MA is a convolution
    * [This example](https://www.datascience.com/blog/python-anomaly-detection) use weighted MA and test both stationary and rolling SD
    * [understand convolution using np.convolve in the above example](https://stackoverflow.com/questions/20036663/understanding-numpys-convolve)
    * [a random ref](https://datascience.stackexchange.com/questions/6547/open-source-anomaly-detection-in-python)
  2. [KNN](https://en.wikipedia.org/wiki/K-nearest_neighbors_algorithm)
* Attempt
  * data1.py apply cumulative MA and stationary SD, which seems not ideal using a single metric from data set 1
  * apply simple MA with a day/week basis seems to generate smoother results
  * data1_allmetric.py modularised function applying cumulative MA and heat map plot with all metrics works
