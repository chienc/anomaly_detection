import json

large_dataset_1 = {}
large_dataset_2 = {}
large_dataset_3 = {}
large_dataset_4 = {}

with open('anomaly_data_z1.out', 'r') as f:
    for line in f:
        datapoints = []
        d = json.loads(line)
        try:
            target = d[0]['target']
            datapoints.extend(d[0]['datapoints'])
            large_dataset_1[target] = datapoints
        except IndexError: # there s a line with no metric and data somewhere
            print(line)
            print(d)

with open('anomaly_data_z2.out', 'r') as f:
    for line in f:
        datapoints = []
        d = json.loads(line)
        target = d[0]['target']
        datapoints.extend(d[0]['datapoints'])
        large_dataset_2[target] = datapoints

with open('anomaly_data_z3.out', 'r') as f:
    for line in f:
        datapoints = []
        d = json.loads(line)
        target = d[0]['target']
        datapoints.extend(d[0]['datapoints'])
        large_dataset_3[target] = datapoints
        
with open('anomaly_data_z4.out', 'r') as f:
    for line in f:
        datapoints = []
        d = json.loads(line)
        target = d[0]['target']
        datapoints.extend(d[0]['datapoints'])
        large_dataset_4[target] = datapoints


# print(len(large_dataset_1['site3.checkout_from_mybasket']))
# print(len(large_dataset_2['site3.checkout_from_mybasket']))
# print(len(large_dataset_3['site3.checkout_from_mybasket']))
# print(len(large_dataset_4['site3.checkout_from_mybasket']))
