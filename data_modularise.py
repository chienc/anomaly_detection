import json
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as md
import itertools
from collections import deque
from pylab import text
import datetime as dt
from pylab import text

def read_data(filename):
    """
    Read in time series data of all metrics from file
    ---
    filename (string): eg: 'anomaly_data_z4.out'
    ---
    return (dictionary): in the format of {metric:[[data_1,timestamp_1], ..., [data_n,timestamp_n]]}
    """
    large_dataset = {}
    with open(filename, 'r') as f:
        for line in f:
            datapoints = []
            d = json.loads(line)
            try:
                target = d[0]['target']
                datapoints.extend(d[0]['datapoints'])
                large_dataset[target] = datapoints
            except IndexError:
                print('skip empty data line')
    return large_dataset

def read_metric(dataset_dictionary,metric):
    """
    extract list of data and list of timestamp from certain
    ---
    dataset_dictionary (dictionary): in the format of {metric:[[data_1,timestamp_1], ..., [data_n,timestamp_n]]}
    metric (string): eg: 'site1.orders_per_checkout'
    ---
    return (dictionary): {'data':[],'timestamp':[]}
    """
    result_dic = {}
    result_dic['data'] = [data[0] if data[0] is not None else np.nan for data in dataset_dictionary[metric]]
    unix_timestamp = [data[1] for data in dataset_dictionary[metric]]
    result_dic['timestamp'] = [dt.datetime.fromtimestamp(ts) for ts in unix_timestamp]
    return result_dic

def simple_moving(data, window_mode):
    """
    calculate simple moving average and standard deviation of input data with selected window mode
    ---
    data (list): specific time series data from certain metric
    window_mode (string): 'day', 'week', 'month', 'season'
    ---
    return (dictionary): {'avg':[],'std':[]}
    """
    result_cal = {}
    if window_mode == 'day':
        num_of_datapoints = 48
        result_cal['avg'] = [0]*(num_of_datapoints-1)
        result_cal['std'] = [0]*(num_of_datapoints-1)
    elif window_mode == 'week':
        num_of_datapoints = 48*7
        result_cal['avg'] = [0]*(num_of_datapoints-1)
        result_cal['std'] = [0]*(num_of_datapoints-1)
    elif window_mode == 'month':
        num_of_datapoints = 48*30
        result_cal['avg'] = [0]*(num_of_datapoints-1)
        result_cal['std'] = [0]*(num_of_datapoints-1)
    elif window_mode == 'season':
        num_of_datapoints = 48*30*3
        result_cal['avg'] = [0]*(num_of_datapoints-1)
        result_cal['std'] = [0]*(num_of_datapoints-1)
    else:
        print('invalid window_mode')

    it = iter(data)
    d = deque(itertools.islice(it, num_of_datapoints-1))
    d.appendleft(0)
    for elem in it:
        d.popleft()
        d.append(elem)
        result_cal['avg'].append(np.nanmean(d))
        result_cal['std'].append(np.nanstd(d))
    return result_cal

def get_deviation(data,avg_list,std_list):
    deviation_list = []
    for element in zip(data,avg_list,std_list):
        if element[2] != 0:
            deviation_list.append(abs(element[0] - element[1])/element[2])
        else:
             deviation_list.append(np.nan)
    return deviation_list

def plotting(filename,metric,sigma=3.0):
    """
    plotting graph
    ---
    filename (string): eg: 'anomaly_data_z4.out'
    metric (string): eg: 'site1.orders_per_checkout'
    sigma (float): deviation threshfold from mean
    """
    large_dataset = read_data(filename)
    data = read_metric(large_dataset,metric)['data']
    timeline = read_metric(large_dataset,metric)['timestamp']

    plt.figure()
    plt.suptitle(metric, fontsize=16)
    for i,v in enumerate(['day', 'week', 'month', 'season']):
        avg_list = simple_moving(data,v)['avg']
        std_list = simple_moving(data,v)['std']
        deviation_list = get_deviation(data,avg_list,std_list)
        anomalies_dict = {(element[0],element[1]): element[2] for element in zip(data,timeline,deviation_list) if element[2] > sigma}
        anom_data = [anom[0] for anom in anomalies_dict.keys()]
        anom_time = [anom[1] for anom in anomalies_dict.keys()]
        percentage = len(anomalies_dict)/len(data)*100
        plt.subplot(2,2,i+1)
        plt.xticks(rotation=50, fontsize=6)
        plt.scatter(timeline, data, marker='x', s=10)
        plt.plot(timeline, avg_list, 'k')
        plt.scatter(anom_time,anom_data,marker = 'x',s=10,c='r')
        plt.fill_between(timeline, np.array(avg_list) + np.array(std_list)*sigma, np.array(avg_list) - np.array(std_list)*sigma, facecolor = "green", alpha = 0.5)
        plt.title("Window mode: " + v)
        plt.xlabel("Time")
        plt.ylabel("Data")
        xfmt = md.DateFormatter('%Y-%b')
        plt.gca().xaxis.set_major_locator(md.MonthLocator())
        plt.gca().xaxis.set_major_formatter(xfmt)
        plt.gca().xaxis.set_minor_locator(md.DayLocator())
        text(0.9, 0.9, str(round(percentage,2)) + '%', horizontalalignment='right', verticalalignment='top', transform = plt.gca().transAxes)
        print(str(round(percentage,2)) + "% of points in " + metric + " are identified as anomalies with a sigma of " + str(sigma) + " in " + v + " window mode")
    plt.tight_layout(rect=[0, 0, 1, 0.9])
    plt.show()

if __name__ == "__main__":
    plotting("anomaly_data_z4.out",'site3.uk_speed')
