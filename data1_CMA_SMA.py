import json
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as md
import itertools
from collections import deque
import datetime as dt

large_dataset_1 = {}
large_dataset_2 = {}
large_dataset_3 = {}
large_dataset_4 = {}

# with open('anomaly_data_z1.out', 'r') as f:
#     for line in f:
#         datapoints = []
#         d = json.loads(line)
#         try:
#             target = d[0]['target']
#             datapoints.extend(d[0]['datapoints'])
#             large_dataset_1[target] = datapoints
#         except IndexError: # there s a line with no metric and data somewhere
#             print('skip empty data line')

# with open('anomaly_data_z2.out', 'r') as f:
#     for line in f:
#         datapoints = []
#         d = json.loads(line)
#         try:
#             target = d[0]['target']
#             datapoints.extend(d[0]['datapoints'])
#             large_dataset_2[target] = datapoints
#         except IndexError:
#             print('skip empty data line')

# with open('anomaly_data_z3.out', 'r') as f:
#     for line in f:
#         datapoints = []
#         d = json.loads(line)
#         try:
#             target = d[0]['target']
#             datapoints.extend(d[0]['datapoints'])
#             large_dataset_3[target] = datapoints
#         except IndexError:
#             print('skip empty data line')
with open('anomaly_data_z4.out', 'r') as f:
    for line in f:
        datapoints = []
        d = json.loads(line)
        try:
            target = d[0]['target']
            datapoints.extend(d[0]['datapoints'])
            large_dataset_4[target] = datapoints
        except IndexError:
            print('skip empty data line')

# # Cumulative moving avg
# cum_moving_avg = np.cumsum(site1_speed_null_to_zero)/np.arange(1,len(site1_speed_null_to_zero)+1)
# std = np.std(site1_speed_no_null)# moving avg std?

# Simple moving avg
def simple_moving_avg(data, num_of_datapoints):
    it = iter(data)
    d = deque(itertools.islice(it, num_of_datapoints-1))
    d.appendleft(0)
    for elem in it:
        d.popleft()
        d.append(elem)
        try:
            yield (np.nanmean(d),np.nanstd(d))
        except:
            yield 0

if __name__ == "__main__":

    # plot the SMA graph
    for k, v in large_dataset_4.items():
        # if 'site1' in k:
        if k == 'site1.orders_per_checkout':
            # Extract datapoints and replace null with 0 for all attributes
            data_no_null = [data[0] if data[0] is not None else np.nan for data in large_dataset_4[k]]
            unix_timestamp = [data[1] for data in large_dataset_4[k]]
            converted_timestamp = [dt.datetime.fromtimestamp(ts) for ts in unix_timestamp] #

            # Simple moving avg
            # Assume 30 days in 1 month
            SMA_day_lst = [0]*(48-1)
            SMA_week_lst = [0]*(48*7-1)
            SMA_month_lst = [0]*(48*30-1)
            SMA_season_lst = [0]*(48*30*3-1)

            std_day_lst = [0]*(48-1)
            std_week_lst = [0]*(48*7-1)
            std_month_lst = [0]*(48*30-1)
            std_season_lst = [0]*(48*30*3-1)

            plt.figure()
            plt.suptitle(k, fontsize=16)

            for cal in simple_moving_avg(data_no_null, 48):
                SMA_day_lst.append(cal[0])
                std_day_lst.append(cal[1])

            plt.subplot(2,2,1)
            plt.xticks(rotation=50, fontsize=6) #
            plt.scatter(converted_timestamp, data_no_null, marker='x', s=10)
            plt.plot(converted_timestamp , SMA_day_lst, 'r')
            plt.fill_between(converted_timestamp, np.array(SMA_day_lst)+np.array(std_day_lst)*3, np.array(SMA_day_lst)-np.array(std_day_lst)*3, facecolor = "green", alpha = 0.5)
            plt.title('SMA_day')
            locator = md.MonthLocator()
            xfmt = md.DateFormatter('%Y-%b')
            plt.gca().xaxis.set_major_locator(locator)
            plt.gca().xaxis.set_major_formatter(xfmt)
            print(std_day_lst[-10:])

            for cal in simple_moving_avg(data_no_null, 48*7):
                SMA_week_lst.append(cal[0])
                std_week_lst.append(cal[1])

            plt.subplot(2,2,2)
            plt.xticks(rotation=50, fontsize=6)
            plt.scatter(converted_timestamp, data_no_null, marker='x', s=10)
            plt.plot(converted_timestamp, SMA_week_lst, 'r')
            plt.fill_between(converted_timestamp, np.array(SMA_week_lst)+np.array(std_week_lst)*3, np.array(SMA_week_lst)-np.array(std_week_lst)*3, facecolor = "green", alpha = 0.5)
            plt.title('SMA_week')
            locator = md.MonthLocator()
            xfmt = md.DateFormatter('%Y-%b')
            plt.gca().xaxis.set_major_locator(locator)
            plt.gca().xaxis.set_major_formatter(xfmt)
            print(2)

            for cal in simple_moving_avg(data_no_null, 48*30):
                SMA_month_lst.append(cal[0])
                std_month_lst.append(cal[1])

            plt.subplot(2,2,3)
            plt.xticks(rotation=50, fontsize=6)
            plt.scatter(converted_timestamp, data_no_null, marker='x', s=10)
            plt.plot(converted_timestamp, SMA_month_lst, 'r')
            plt.fill_between(converted_timestamp, np.array(SMA_month_lst)+np.array(std_month_lst)*3, np.array(SMA_month_lst)-np.array(std_month_lst)*3, facecolor = "green", alpha = 0.5)
            plt.title('SMA_month')
            locator = md.MonthLocator()
            xfmt = md.DateFormatter('%Y-%b')
            plt.gca().xaxis.set_major_locator(locator)
            plt.gca().xaxis.set_major_formatter(xfmt)
            print(3)

            for cal in simple_moving_avg(data_no_null, 48*30*3):
                SMA_season_lst.append(cal[0])
                std_season_lst.append(cal[1])

            plt.subplot(2,2,4)
            plt.xticks(rotation=50, fontsize=6)
            plt.scatter(converted_timestamp, data_no_null, marker='x', s=10)
            plt.plot(converted_timestamp, SMA_season_lst, 'r')
            plt.fill_between(converted_timestamp, np.array(SMA_season_lst)+np.array(std_season_lst)*3, np.array(SMA_season_lst)-np.array(std_season_lst)*3, facecolor = "green", alpha = 0.5)
            plt.title('SMA_season')
            locator = md.MonthLocator()
            xfmt = md.DateFormatter('%Y-%b')
            plt.gca().xaxis.set_major_locator(locator)
            plt.gca().xaxis.set_major_formatter(xfmt)
            print(len(std_season_lst))

            plt.show()
